package org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.partitioners;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.lib.partition.HashPartitioner;
import org.apache.pig.PigConfiguration;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.backend.hadoop.HDataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.io.NullableTuple;
import org.apache.pig.impl.io.PigNullableWritable;

public class RollupH2IRGAutoPivotPartitioner extends
        HashPartitioner<PigNullableWritable, Writable> implements Configurable {

    protected MessageDigest m = null;
    protected int choosen = 0;
    protected HashMap<String, Integer> hm;
    protected int pivot = 0;
    protected int rollupFieldIndex = 0;
    protected int rollupOldFieldIndex = 0;
    protected boolean pivotZero = false;
    protected int length = 0;
    public static long tLookup = 0;
    public static long hashTime = 0;
    public long sTime = 0;
    protected HashMap<Integer, Integer> mrcube = new HashMap<Integer, Integer>();
    protected Tuple t;
    protected TupleFactory mTupleFactory = TupleFactory.getInstance();
    public RollupH2IRGAutoPivotPartitioner() throws NoSuchAlgorithmException {
        //m = MessageDigest.getInstance("MD5");
    }

    @Override
    public Configuration getConf() {
        return null;
    }

    @Override
    public int getPartition(PigNullableWritable key, Writable value,
            int numPartitions) {
        int count = 0;
        for (int i = 0; i < ((Tuple) key.getValueAsPigType()).size(); i++)
            try {
                if (((Tuple) key.getValueAsPigType()).get(i) == null)
                    count++;
            } catch (ExecException e) {
                e.printStackTrace();
            }
        
        t = mTupleFactory.newTuple();
        
        for (int i = 0; i < length; i++)
            t.append(null);
        
        int stopIndex = 0;
        
        if (count != 0)
            return (key.hashCode() & Integer.MAX_VALUE) % numPartitions;
        
        stopIndex = ((Tuple) key.getValueAsPigType()).size() - mrcube.get(count) - 1;
        for (int i = 0; i < stopIndex; i++) {
            try {
                t.set(i, ((Tuple) key.getValueAsPigType()).get(i));
            } catch (Exception e) {

            }
        }
        Integer num = hm.get(t.toString());

        if (num == null)
            return (t.hashCode() & Integer.MAX_VALUE) % numPartitions;

        return num;
    }

    @Override
    public void setConf(Configuration conf) {
        Path pPivot = new Path("/tmp/partition/mrcube");
        FileSystem fs;
        length = conf.getInt(PigConfiguration.PIG_H2IRG_ROLLUP_TOTAL_FIELD, 0);
        try {
            fs = FileSystem.get(conf);
            BufferedReader br = new BufferedReader(new InputStreamReader(fs
                    .open(pPivot)));
            String line = null;
            while ((line = br.readLine()) != null) {
                String splitted[] = line.split(" ");
                mrcube.put(Integer.parseInt(splitted[2]), Integer.parseInt(splitted[splitted.length - 1]));
            }
            br.close();
            Path[] localFiles = DistributedCache.getLocalCacheFiles(conf);
            BufferedReader br2 = new BufferedReader(new InputStreamReader(FileSystem.getLocal(conf).open(localFiles[0])));
            line = br2.readLine();
            
            if (hm == null)
                hm = new HashMap<String, Integer>();
            hm.clear();
            
            while (line != null) {
                String[] vals = line.toString().split("\t");
                //if(vals.length > 1) {
                TupleFactory mTupleFactory = TupleFactory.getInstance();
                Tuple t = mTupleFactory.newTuple();
                for (int i = 0; i < vals.length - 1; i++) {
                    if (vals[i].equals("null"))
                        t.append(null);
                    else
                        t.append(vals[i]);
                }
                int reducerNo = Integer.parseInt(vals[vals.length - 1]);
                hm.put(t.toString(), new Integer(reducerNo));
                //}
                line = br2.readLine();
            }
            br2.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
