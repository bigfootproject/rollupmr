package org.apache.pig.newplan.logical.rules;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.pig.Algebraic;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataType;
import org.apache.pig.impl.PigContext;
import org.apache.pig.impl.logicalLayer.FrontendException;
import org.apache.pig.newplan.Operator;
import org.apache.pig.newplan.OperatorPlan;
import org.apache.pig.newplan.OperatorSubPlan;
import org.apache.pig.newplan.logical.expression.CastExpression;
import org.apache.pig.newplan.logical.expression.DereferenceExpression;
import org.apache.pig.newplan.logical.expression.LogicalExpression;
import org.apache.pig.newplan.logical.expression.LogicalExpressionPlan;
import org.apache.pig.newplan.logical.expression.ProjectExpression;
import org.apache.pig.newplan.logical.expression.UserFuncExpression;
import org.apache.pig.newplan.logical.relational.LOCogroup;
import org.apache.pig.newplan.logical.relational.LOCube;
import org.apache.pig.newplan.logical.relational.LOForEach;
import org.apache.pig.newplan.logical.relational.LOGenerate;
import org.apache.pig.newplan.logical.relational.LOHForEach;
import org.apache.pig.newplan.logical.relational.LOInnerLoad;
import org.apache.pig.newplan.logical.relational.LogicalPlan;
import org.apache.pig.newplan.logical.relational.LogicalSchema;
import org.apache.pig.newplan.logical.relational.LogicalSchema.LogicalFieldSchema;
import org.apache.pig.newplan.optimizer.Rule;
import org.apache.pig.newplan.optimizer.Transformer;

public class RollupOptimizationRule extends Rule {

    private boolean bDebugInfo = false;

    private OperatorSubPlan subPlan;

    public RollupOptimizationRule(String name) {
        super(name, false);
    }

    @Override
    protected OperatorPlan buildPattern() {
        // Construct the pattern plan for RollupOptimization
        // The pattern plan is as below:
        // LOForEach
        // |
        // |
        // LOCogroup
        // |
        // |
        // LOCube

        LogicalPlan lp = new LogicalPlan();
        LOForEach foreach = new LOForEach(lp);
        LOCogroup groupby = new LOCogroup(lp);
        // LOCube cube = new LOCube(lp);
        lp.add(foreach);
        lp.add(groupby);
        // lp.add(cube);
        lp.connect(foreach, groupby);
        // lp.connect(groupby, cube);
        return lp;
    }

    @Override
    public Transformer getNewTransformer() {
        return new RollupTransformer();
    }

    private void debug(String s) {
        if (bDebugInfo)
            log.info(s);
    }

    public class RollupTransformer extends Transformer {

        // User defined schema for generate operator. If not specified output schema
        // of UDF will be used which will prefix "dimensions" namespace to all fields
        private List<LogicalSchema> getUserDefinedSchema(List<LogicalExpressionPlan> allExprPlan)
                throws FrontendException {
            List<LogicalSchema> genOutputSchema = new ArrayList<LogicalSchema>();
            for (int i = 0; i < allExprPlan.size(); i++) {
                List<Operator> opers = allExprPlan.get(i).getSources();
                for (Operator oper : opers) {

                    // add a logical schema for dimensions that are pushed from
                    // predecessor of cube/rollup
                    if (oper instanceof ProjectExpression) {
                        LogicalSchema output = new LogicalSchema();
                        output.addField(new LogicalFieldSchema(((ProjectExpression) oper).getColAlias(), null,
                                DataType.NULL));
                        genOutputSchema.add(output);
                    } else if (oper instanceof UserFuncExpression) {
                        // add logical schema for dimensions specified in
                        // cube/rollup operator
                        LogicalSchema output = new LogicalSchema();
                        for (Operator op : ((UserFuncExpression) oper).getPlan().getSinks()) {
                            output.addField(new LogicalFieldSchema(((ProjectExpression) op).getFieldSchema()));
                        }
                        genOutputSchema.add(output);
                    } else if (oper instanceof CastExpression) {
                        LogicalSchema output = new LogicalSchema();
                        output.addField(new LogicalFieldSchema(((CastExpression) oper).getFieldSchema()));
                        genOutputSchema.add(output);
                    } else if (oper instanceof DereferenceExpression) {
                        LogicalSchema output = new LogicalSchema();
                        output.addField(new LogicalFieldSchema(((ProjectExpression) ((DereferenceExpression) oper)
                                .getReferredExpression()).getColAlias(), null, DataType.NULL));
                        genOutputSchema.add(output);
                    }

                }
            }
            return genOutputSchema;
        }

        @Override
        public boolean check(OperatorPlan matched) throws FrontendException {
            debug("Rollup Optimization: Found matched Rollup Optimation Rule");

            // check if the Rollup pattern is followed by FOREACH with an
            // algebraic function apply to the group
            // If it is the case we can apply the optimizer
            LOForEach foreach1 = (LOForEach) matched.getSources().get(0);
            LOCogroup cogroup = (LOCogroup) matched.getSuccessors(foreach1).get(0);

            if (currentPlan.getSuccessors(cogroup) == null)
                return false;

            // check if it is Rollup
            boolean bRollup = false;

            Iterator<Operator> it = foreach1.getInnerPlan().getOperators();
            while (it.hasNext()) {
                Operator op = it.next();
                debug("information lvl 0: " + op.getName() + " | " + op.getClass() + " | " + op.toString());
                if (op instanceof LOGenerate) {
                    debug("LOGenerate schema lvl 0: " + ((LOGenerate) op).getSchema());
                    List<LogicalExpressionPlan> inner_leplan = ((LOGenerate) op).getOutputPlans();
                    for (LogicalExpressionPlan op2 : inner_leplan) {
                        debug("information lvl 1: " + op2.getClass() + " | " + op2.toString());
                        for (Operator op3 : op2.getSources()) {
                            if (op3 instanceof UserFuncExpression) {
                                debug("information lvl 2: " + op3.getName() + " | " + op3.getClass() + " | "
                                        + op3.toString());
                                UserFuncExpression uf = (UserFuncExpression) op3;
                                debug("Field schema: " + uf.getFieldSchema());
                                debug("FuncSpec: " + uf.getFuncSpec().toString());
                                if (uf.getFuncSpec().toString().equals("org.apache.pig.builtin.RollupDimensions")) {
                                    bRollup = true;
                                }
                                List<LogicalExpression> inpUfs = uf.getArguments();
                                for (LogicalExpression op4 : inpUfs) {
                                    if (op4 instanceof ProjectExpression) {
                                        ProjectExpression pe = (ProjectExpression) op4;
                                        debug("information lvl 3: " + pe.getName() + " | " + pe.getClass() + " | "
                                                + pe.getColAlias() + " | " + pe.getInputNum() + " | "
                                                + pe.getStartCol() + " | " + pe.getEndCol() + " | "
                                                + pe.getAttachedRelationalOp().getClass() + " | " + pe.getFieldSchema());
                                    } else if (op4 instanceof CastExpression) {
                                        CastExpression ce = (CastExpression) op4;
                                        debug("information lvl 3: " + ce.getName() + " | " + ce.getClass() + " | "
                                                + ce.getFieldSchema() + " | " + ce.getExpression() + " | "
                                                + ce.getFuncSpec());
                                        debug("*** : " + op2.getSuccessors(ce).get(0));
                                        // debug("*** : " + ce.getPlan());
                                    } else {
                                        debug("information lvl 3: " + op4.getName() + " | " + op4.getClass() + " | "
                                                + op4.getFieldSchema());
                                    }
                                }
                            } else if (op3 instanceof ProjectExpression) {
                                ProjectExpression pe = (ProjectExpression) op3;
                                debug("information lvl 2: " + pe.getName() + " | " + pe.getClass() + " | "
                                        + pe.getColAlias() + " | " + pe.getInputNum() + " | " + pe.getStartCol()
                                        + " | " + pe.getEndCol() + " | " + pe.getAttachedRelationalOp().getClass()
                                        + " | " + pe.getFieldSchema());

                            } else if (op3 instanceof CastExpression) {
                                CastExpression ce = (CastExpression) op3;
                                debug("information lvl 2: " + ce.getName() + " | " + ce.getClass() + " | "
                                        + ce.getFieldSchema() + " | " + ce.getExpression() + " | " + ce.getFuncSpec());
                                debug("*** : " + op2.getSuccessors(ce).get(0));
                                debug("*** : " + ce.getPlan());
                            } else {
                                debug("information lvl 2: " + op3.getName() + " | " + op3.getClass() + " | "
                                        + op3.toString());
                            }
                        }
                    }

                } else if (op instanceof LOInnerLoad) {
                    LOInnerLoad iltmp = (LOInnerLoad) op;
                    ProjectExpression pe = iltmp.getProjection();
                    debug("information lvl 1: " + pe.getName() + " | " + pe.getClass() + " | " + pe.getColAlias()
                            + " | " + pe.getInputNum() + " | " + pe.getStartCol() + " | " + pe.getEndCol() + " | "
                            + pe.getAttachedRelationalOp().getClass() + " | " + pe.getFieldSchema());
                }
            }

            if (!bRollup)
                return false;

            if (currentPlan.getSuccessors(cogroup) == null)
                return false;
            if (!(currentPlan.getSuccessors(cogroup).get(0) instanceof LOCube))
                return false;
            LOCube cube = (LOCube) currentPlan.getSuccessors(cogroup).get(0);
            List<Operator> succs = currentPlan.getSuccessors(cube);

            // check if the optimization if applicable with the function
            boolean bOptimization = false;

            if (!succs.isEmpty() && succs.size() == 1) {
                if (succs.get(0) instanceof LOForEach) {
                    debug(" +++++++++++++++++++++++++++++++++++ ");
                    LOForEach foreach2 = (LOForEach) succs.get(0);
                    it = foreach2.getInnerPlan().getOperators();
                    while (it.hasNext()) {
                        Operator op = it.next();
                        debug("information lvl 0: " + op.toString());
                        if (op instanceof LOGenerate) {
                            debug("LOGenerate schema lvl 0 of ForEach2: " + ((LOGenerate) op).getSchema());
                            List<LogicalExpressionPlan> inner_leplan = ((LOGenerate) op).getOutputPlans();
                            for (LogicalExpressionPlan loplan2 : inner_leplan) {
                                debug("information lvl 1: " + loplan2.toString());
                                for (Operator op3 : loplan2.getSources()) {
                                    if (op3 instanceof UserFuncExpression) {
                                        debug("information lvl 2: " + op3.getName() + " | " + op3.getClass() + " | "
                                                + op3.toString());
                                        UserFuncExpression uf = (UserFuncExpression) op3;
                                        debug("Field schema: " + uf.getFieldSchema());
                                        debug("FuncSpec: " + uf.getFuncSpec());
                                        EvalFunc<?> ef = (EvalFunc<?>) PigContext.instantiateFuncFromSpec(uf
                                                .getFuncSpec());

                                        // check if the evaluate function is algebraic so that we
                                        // can apply our optimization
                                        if (ef instanceof Algebraic) {
                                            bOptimization = true;
                                            Operator op4 = loplan2.getSuccessors(op3).get(0);
                                            if (op4 instanceof DereferenceExpression) {
                                                DereferenceExpression deref = (DereferenceExpression) op4;
                                                debug("Deref: " + deref.toString() + " | " + deref.getFieldSchema()
                                                        + " | " + deref.getNextUid() + " | "
                                                        + deref.getReferredExpression().toString());
                                            }
                                        }
                                    } else {
                                        debug("information lvl 2: " + op3.getName() + " | " + op3.getClass() + " | "
                                                + op3.toString());
                                    }
                                }
                            }

                        }
                    }
                }
            }

            // return false;
            return bOptimization;
        }

        @Override
        public void transform(OperatorPlan matched) throws FrontendException {
            // TODO Auto-generated method stub
            debug("Transform: do something");

            subPlan = new OperatorSubPlan(currentPlan);

            LOForEach foreach1 = (LOForEach) matched.getSources().get(0);

            LOCogroup cogroup = (LOCogroup) matched.getSuccessors(foreach1).get(0);
            LOCube cube = (LOCube) currentPlan.getSuccessors(cogroup).get(0);
            List<Operator> succs = currentPlan.getSuccessors(cube);
            LOForEach foreach2 = (LOForEach) succs.get(0);

            Iterator<Operator> it = foreach1.getInnerPlan().getOperators();
            while (it.hasNext()) {
                Operator op = it.next();
                if (op instanceof LOGenerate) {
                    List<LogicalExpressionPlan> inner_leplan = ((LOGenerate) op).getOutputPlans();
                    // List<LogicalExpressionPlan> list_new_le_plan = new
                    // ArrayList<LogicalExpressionPlan>();
                    LogicalExpressionPlan userfunc = null;

                    for (LogicalExpressionPlan op2 : inner_leplan) {
                        // boolean isAdded = true;
                        for (Operator op3 : op2.getSources()) {
                            if (op3 instanceof UserFuncExpression) {
                                UserFuncExpression uf = (UserFuncExpression) op3;
                                if (uf.getFuncSpec().toString().equals("org.apache.pig.builtin.RollupDimensions")) {
                                    // isAdded = false;
                                    // List<LogicalExpression> inpUfs = uf.getArguments();
                                    // for (LogicalExpression op4 : inpUfs) {
                                    // LogicalExpressionPlan new_leplan = new
                                    // LogicalExpressionPlan();
                                    // LogicalExpression new_le = op4.deepCopy(new_leplan);
                                    // new_leplan.add(new_le);
                                    // list_new_le_plan.add(new_leplan);
                                    // }
                                    userfunc = op2;

                                }
                            }
                        }
                        // if (isAdded)
                        // list_new_le_plan.add(op2);
                    }
                    if (userfunc != null) {
                        // debug("LEPLan: " + list_new_le_plan);
                        // ((LOGenerate) op).setOutputPlans(list_new_le_plan);
                        // ((LOGenerate)
                        // op).setUserDefinedSchema(getUserDefinedSchema(list_new_le_plan));
                        // debug("LOGenerate userDefinedSchema: " + ((LOGenerate)
                        // op).getUserDefinedSchema().size());
                        // List<Boolean> flattenFlags = new ArrayList<Boolean>();
                        // for (int idx = 0; idx < list_new_le_plan.size(); idx++) {
                        // List<Operator> opers = list_new_le_plan.get(idx).getSources();
                        // for (Operator oper : opers) {
                        // if (oper instanceof ProjectExpression) {
                        // flattenFlags.add(false);
                        // } else if (oper instanceof UserFuncExpression) {
                        // flattenFlags.add(true);
                        // } else if (oper instanceof CastExpression) {
                        // flattenFlags.add(false);
                        // } else if (oper instanceof DereferenceExpression) {
                        // flattenFlags.add(false);
                        // }
                        // }
                        // }
                        // boolean[] flags = new boolean[flattenFlags.size()];
                        // for (int i = 0; i < flattenFlags.size(); i++)
                        // flags[i] = flattenFlags.get(i);
                        // ((LOGenerate) op).setFlattenFlags(flags);
                        cogroup.setCustomPartitioner("org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.partitioners.RollupHybridPartitioner");
                        cogroup.setRequestedParallelism(cube.getRequestedParallelism());
                        currentPlan.removeAndReconnect(cube);

                        LOHForEach hfe = new LOHForEach(foreach2);
                        hfe.setPartitionIndex(0);

                        debug("LOHForEach: " + hfe.getInnerPlan());

                        debug("succs size: " + succs.size());
                        debug("ForEach2: " + foreach2.getName());
                        currentPlan.replace(foreach2, hfe);

                        subPlan.add(foreach1);
                        subPlan.add(hfe);
                        break;
                    }
                }
            }

            debug("Transform: done");
        }

        @Override
        public OperatorPlan reportChanges() {

            // TODO Auto-generated method stub
            return subPlan;
        }

    }
}
