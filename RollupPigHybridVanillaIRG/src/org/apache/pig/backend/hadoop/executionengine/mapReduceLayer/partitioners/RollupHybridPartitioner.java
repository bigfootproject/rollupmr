package org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.partitioners;

import org.apache.hadoop.conf.Configurable;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.lib.partition.HashPartitioner;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.backend.hadoop.HDataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.io.PigNullableWritable;

public class RollupHybridPartitioner extends HashPartitioner<PigNullableWritable, Writable> implements
		Configurable {
	Byte kt;
	int index;
	int hash;
	Tuple t;
	Object tmp;
	PigNullableWritable realKey;
	int i;
	String hashtmp;

	public RollupHybridPartitioner() {
		index = 0;
	}

	public int getPartition(PigNullableWritable key, Writable value,
			int numPartitions) {		
		try {
			t = (Tuple) key.getValueAsPigType();
			
			tmp = t.get(index);		
			if (tmp != null) {
				hashtmp = "";				
				for (i = 0; i <= index; i++) {
					kt = Byte.valueOf(t.getType(i));
					realKey = HDataType.getWritableComparableTypes(
							t.get(i), kt.byteValue());
					hashtmp += realKey.hashCode();
					//System.out.println("Partitioner: realKey " + realKey + ", hash: " + hash);
				}
				hash = hashtmp.hashCode();
//				kt = Byte.valueOf(t.getType(0));
//				realKey = HDataType.getWritableComparableTypes(t.get(0), kt.byteValue());
//				hash = (Integer)realKey.getValueAsPigType();
				
			} else {
				hash = t.hashCode();
			}
			return (hash & 0x7fffffff) % numPartitions;
			
		} catch (ExecException e) {
			throw new RuntimeException(e);
		}
	}

	public Configuration getConf() {
		throw new UnsupportedOperationException();
	}

	public void setConf(Configuration conf) {
		String sindex = conf.get("pig.partitioner.hybrid.index", "0");
		index = Integer.valueOf(sindex).intValue();		
	}
}