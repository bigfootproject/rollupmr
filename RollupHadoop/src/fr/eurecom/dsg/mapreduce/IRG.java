package fr.eurecom.dsg.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 * 
 */
public class IRG extends Configured implements Tool {

	private Path inputPath;
	private Path outputDir;

	@Override
	public int run(String[] args) throws Exception {

		Configuration conf = this.getConf();
		Job job = new Job(conf, "Hung-IRGApproach"); 
		
		// set job input format
		job.setInputFormatClass(SequenceFileInputFormat.class);

		// set map class and the map output key and value classes
		job.setMapperClass(IRGMapper.class);
		job.setMapOutputKeyClass(TimestampWritable.class);
		job.setMapOutputValueClass(LongWritable.class);
		job.setPartitionerClass(HashPartitioner.class);

		// set reduce class and the reduce output key and value classes
		job.setReducerClass(IRGReducer.class);
		
		//job.setSortComparatorClass(TimestampWritable.Comparator.class);

		// set job output format
		job.setOutputKeyClass(TimestampWritable.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		// job.setCombinerClass(IRGCombiner.class);

		// add the input file as job input (from HDFS) to the variable
		// inputFile
		FileInputFormat.addInputPath(job, inputPath);

		// set the output path for the job results (to HDFS) to the
		// variable
		// outputPath
		FileOutputFormat.setOutputPath(job, outputDir);

		// set the number of reducers using variable numberReducers
		job.setNumReduceTasks(1);

		// set the jar class
		job.setJarByClass(IRG.class);

		return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
	}

	public IRG(String[] args) {
		if (args.length != 2) {
			System.out
					.println("Usage: IRGImplementation <input_path> <output_path>");
			System.exit(0);
		}
		this.inputPath = new Path(args[0]);
		this.outputDir = new Path(args[1]);
	}

	public static void main(String args[]) throws Exception {
		int res = ToolRunner
				.run(new Configuration(), new IRG(args), args);
		System.exit(res);
	}

}


class IRGMapper extends Mapper<TimestampWritable, LongWritable, TimestampWritable, LongWritable> { 

	public IRGMapper() {

	}

	@Override
	protected void map(TimestampWritable key, 
	        LongWritable value,
			Context context) throws IOException, InterruptedException {
	    context.write(key, value);
	}

}

class IRGCombiner extends Reducer<TimestampWritable,
        LongWritable,
        TimestampWritable,
        LongWritable> {
    
    public IRGCombiner() {

    }

    @Override
    protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
            throws IOException, InterruptedException {
        // implement the reduce method (use context.write to emit results)
        long sum = 0;
        for (LongWritable lw : value) {
            sum += lw.get();
        }
        context.write(key, new LongWritable(sum));
    }
        
}

class IRGReducer extends Reducer<TimestampWritable,
        LongWritable, 
        TimestampWritable, 
        LongWritable> { 

    TimestampWritable previousKey;
    long[] tmpRes;
    
	public IRGReducer() {
	    previousKey = null;
	    tmpRes = new long[TimestampWritable.length];
	}
	
	@Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
	    //System.out.println("cleanup");
	    int x = TimestampWritable.NullValue;
	    compute(new TimestampWritable(x, x, x, x, x, x), new LongWritable(0), context);
	    context.write(new TimestampWritable(x, x, x, x, x, x), new LongWritable(tmpRes[0]));
        super.cleanup(context);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        previousKey = new TimestampWritable(null);
        for (int i = 0; i < TimestampWritable.length; i++) {
            tmpRes[i] = 0;
        }
    }
    
    protected void compute(TimestampWritable key, LongWritable value, Context context)
            throws IOException, InterruptedException {

        if (previousKey.fields != null) {
            LongWritable tmpVal = new LongWritable();
            for (int i = 0; i < TimestampWritable.length; i++) {
                if (previousKey.fields[i] != key.fields[i]) {
                    for (int j = TimestampWritable.length - 2; j >= i; j--) {
                        tmpRes[j] += tmpRes[j+1];
                        tmpVal.set(tmpRes[j+1]);
                        tmpRes[j+1] = 0;
                        previousKey.fields[j+1] = TimestampWritable.NullValue;
                        //System.out.println("O1:\t" + previousKey + " / " + tmpVal);
                        context.write(previousKey, tmpVal);
                    }
                    break;
                }
            }
        }
        
        if (key.fields[0] != TimestampWritable.NullValue) {
            //System.out.println("O2:\t" + key + " / " + value);
            context.write(key, value);
            tmpRes[TimestampWritable.length - 1] += value.get();
            previousKey.fields = key.fields.clone();
        }
    }

    @Override
	protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
			throws IOException, InterruptedException {
		// implement the reduce method (use context.write to emit results)
		long sum = 0;
		for (LongWritable lw : value) {
		    sum += lw.get();
		}
		compute(key, new LongWritable(sum), context);
	}
}