package fr.eurecom.dsg.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 * 
 */
public class HybridIRGVanilla extends Configured implements Tool {

    private int numReducers;
	private Path inputPath;
	private Path outputDir;
	private int pivot;

	@Override
	public int run(String[] args) throws Exception {

		Configuration conf = this.getConf();
		Job job = new Job(conf, "Hung-Hybrid-IRG-Vanilla-" + this.pivot); 
		
		// set job input format
		job.setInputFormatClass(SequenceFileInputFormat.class);

		// set map class and the map output key and value classes
		job.setMapperClass(HybridIRGVanillaMapper.class);
		job.setMapOutputKeyClass(TimestampWritable.class);
		job.setMapOutputValueClass(LongWritable.class);
		
		job.setPartitionerClass(HybridIRGVanillaPartitioner.class);

		// set reduce class and the reduce output key and value classes
		job.setReducerClass(HybridIRGVanillaReducer.class);
		
		//job.setSortComparatorClass(TimestampWritable.Comparator.class);

		// set job output format
		job.setOutputKeyClass(TimestampWritable.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		job.setCombinerClass(HybridIRGVanillaCombiner.class);
		

		// add the input file as job input (from HDFS) to the variable
		// inputFile
		FileInputFormat.addInputPath(job, inputPath);

		// set the output path for the job results (to HDFS) to the
		// variable
		// outputPath
		FileOutputFormat.setOutputPath(job, outputDir);

		// set the number of reducers using variable numberReducers
		job.setNumReduceTasks(this.numReducers);

		// set the jar class
		job.setJarByClass(HybridIRGVanilla.class);
		
		job.getConfiguration().set("hybrid.pivot", String.valueOf(pivot));

		return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
	}

	public HybridIRGVanilla(String[] args) {
		if (args.length != 4) {
			System.out
					.println("Usage: HybridImplementation <input_path> <output_path> <pivot> <num_reducers>");
			System.exit(0);
		}
		this.inputPath = new Path(args[0]);
		this.outputDir = new Path(args[1]);
		this.pivot = Integer.parseInt(args[2]);
		this.numReducers = Integer.parseInt(args[3]);
	}

	public static void main(String args[]) throws Exception {
		int res = ToolRunner
				.run(new Configuration(), new HybridIRGVanilla(args), args);
		System.exit(res);
	}

}


class HybridIRGVanillaMapper extends Mapper<TimestampWritable, LongWritable, TimestampWritable, LongWritable> { 

	public HybridIRGVanillaMapper() {

	}
	
	protected int pivot = -1;

	@Override
    protected void setup(org.apache.hadoop.mapreduce.Mapper<TimestampWritable, LongWritable, TimestampWritable, LongWritable>.Context context) throws IOException, InterruptedException {
        super.setup(context);
        pivot = Integer.parseInt(context.getConfiguration().get("hybrid.pivot", "-1"));
    }


    @Override
	protected void map(TimestampWritable key, 
	        LongWritable value,
			Context context) throws IOException, InterruptedException {
	    context.write(key, value);
	    
	    for (int i = pivot + 1; i < TimestampWritable.length; i++) {
            key.fields[i] = TimestampWritable.NullValue;
        }
	    
	    for (int i = pivot; i >= 0; i--) {
	        key.fields[i] = TimestampWritable.NullValue;
	        context.write(key, value);
	    }
	}

}

class HybridIRGVanillaCombiner extends Reducer<TimestampWritable,
        LongWritable,
        TimestampWritable,
        LongWritable> {
    
    public HybridIRGVanillaCombiner() {

    }

    @Override
    protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
            throws IOException, InterruptedException {
        // implement the reduce method (use context.write to emit results)
        long sum = 0;
        for (LongWritable lw : value) {
            sum += lw.get();
        }
        context.write(key, new LongWritable(sum));
    }
        
}

class HybridIRGVanillaReducer extends Reducer<TimestampWritable,
        LongWritable, 
        TimestampWritable, 
        LongWritable> { 

    protected TimestampWritable previousKey;
    protected long[] tmpRes;
    protected int pivot;
    
	public HybridIRGVanillaReducer() {
	    previousKey = null;
	    tmpRes = new long[TimestampWritable.length];
	}
	
	@Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
	    //System.out.println("cleanup");
	    compute(new TimestampWritable(-1, -1, -1, -1, -1, -1), new LongWritable(0), context);
        super.cleanup(context);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        previousKey = new TimestampWritable(null);
        for (int i = 0; i < TimestampWritable.length; i++) {
            tmpRes[i] = 0;
        }
        pivot = Integer.parseInt(context.getConfiguration().get("hybrid.pivot", "-1"));
    }
    
    protected void compute(TimestampWritable key, LongWritable value, Context context)
            throws IOException, InterruptedException {

        if (previousKey.fields != null) {
            LongWritable tmpVal = new LongWritable();
            for (int i = 0; i < TimestampWritable.length; i++) {
                if (previousKey.fields[i] != key.fields[i]) {
                    int x = Math.max(i, pivot);
                    for (int j = TimestampWritable.length - 2; j >= x; j--) {
                        tmpRes[j] += tmpRes[j+1];
                        tmpVal.set(tmpRes[j+1]);
                        tmpRes[j+1] = 0;
                        previousKey.fields[j+1] = TimestampWritable.NullValue;
                        //System.out.println("O1:\t" + previousKey + " / " + tmpVal);
                        context.write(previousKey, tmpVal);
                    }
                    break;
                }
                
            }
        }
        if (key.fields[0] != -1) {
            //System.out.println("O2:\t" + key + " / " + value);
            context.write(key, value);
            tmpRes[TimestampWritable.length - 1] += value.get();
            previousKey.fields = key.fields.clone();
        }
    }

    @Override
	protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
			throws IOException, InterruptedException {
		// implement the reduce method (use context.write to emit results)
		long sum = 0;
		for (LongWritable lw : value) {
		    sum += lw.get();
		}
		if (key.fields[pivot] != TimestampWritable.NullValue) {
		    compute(key, new LongWritable(sum), context);
		} else {
		    context.write(key, new LongWritable(sum));
		}
	}
}
