package fr.eurecom.dsg.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 * 
 */
public class Naive extends Configured implements Tool {

	private int numReducers;
	private Path inputPath;
	private Path outputDir;

	@Override
	public int run(String[] args) throws Exception {

		Configuration conf = this.getConf();
		Job job = new Job(conf, "Hung-NaiveNoCombiner"); 
		
		// set job input format
		job.setInputFormatClass(SequenceFileInputFormat.class);

		// set map class and the map output key and value classes
		job.setMapperClass(NaiveMapper.class);
		job.setMapOutputKeyClass(TimestampWritable.class);
		job.setMapOutputValueClass(LongWritable.class);
		job.setPartitionerClass(HashPartitioner.class);

		// set reduce class and the reduce output key and value classes
		job.setReducerClass(NaiveReducer.class);
		
		//job.setSortComparatorClass(TimestampWritable.Comparator.class);

		// set job output format
		job.setOutputKeyClass(TimestampWritable.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		job.setCombinerClass(NaiveReducer.class);

		// add the input file as job input (from HDFS) to the variable
		// inputFile
		FileInputFormat.addInputPath(job, inputPath);

		// set the output path for the job results (to HDFS) to the
		// variable
		// outputPath
		FileOutputFormat.setOutputPath(job, outputDir);

		// set the number of reducers using variable numberReducers
		job.setNumReduceTasks(this.numReducers);

		// set the jar class
		job.setJarByClass(Naive.class);

		return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
	}

	public Naive(String[] args) {
		if (args.length != 3) {
			System.out
					.println("Usage: NaiveImplementation <input_path> <output_path> <num_reducers> ");
			System.exit(0);
		}
		this.inputPath = new Path(args[0]);
		this.outputDir = new Path(args[1]);
		this.numReducers = Integer.parseInt(args[2]);
	}

	public static void main(String args[]) throws Exception {
		int res = ToolRunner
				.run(new Configuration(), new Naive(args), args);
		System.exit(res);
	}

}


class NaiveMapper extends Mapper<TimestampWritable, LongWritable, TimestampWritable, LongWritable> { 

	public NaiveMapper() {

	}

	@Override
	protected void map(TimestampWritable key, // change Object to input key type
	        LongWritable value, // change Object to input value type
			Context context) throws IOException, InterruptedException {
	    context.write(key, value);
	    for (int i = TimestampWritable.length - 1; i >= 0; i--) {
	        key.fields[i] = TimestampWritable.NullValue;
	        context.write(key, value);
	    }
	}

}

class NaiveReducer extends Reducer<TimestampWritable, // change Object to input key type
        LongWritable, // change Object to input value type
        TimestampWritable, // change Object to output key type
        LongWritable> { // change Object to output value type

	public NaiveReducer() {

	}

	@Override
	protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
			throws IOException, InterruptedException {
		// implement the reduce method (use context.write to emit results)
		long sum = 0;
		for (LongWritable lw : value) {
		    sum += lw.get();
		}
		context.write(key, new LongWritable(sum));
	}
}
