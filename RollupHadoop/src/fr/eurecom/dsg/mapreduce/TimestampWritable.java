package fr.eurecom.dsg.mapreduce;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class TimestampWritable implements WritableComparable<TimestampWritable> {
    
    public static int length = 6;
    public static int NullValue = -1;
    public int[] fields = new int[length];
        
    public TimestampWritable() {    
    }
    
    public TimestampWritable(int[] f) {
        fields = f;
    }
    
    public TimestampWritable(int y, int m, int d, int h, int MM, int s) {
        fields[0] = y;
        fields[1] = m;
        fields[2] = d;
        fields[3] = h;
        fields[4] = MM;
        fields[5] = s;
    }
    
    public void set(int[] f) {
        fields = f;
    }
    
    public void set(int y, int m, int d, int h, int MM, int s) {
        fields[0] = y;
        fields[1] = m;
        fields[2] = d;
        fields[3] = h;
        fields[4] = MM;
        fields[5] = s;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        for (int i = 0; i < length; i++)
            fields[i] = in.readInt();
    }

    @Override
    public void write(DataOutput out) throws IOException {
        for (int i = 0; i < length; i++)
            out.writeInt(fields[i]);
    }

    @Override
    public int compareTo(TimestampWritable obj) {
        for (int i = 0; i < length; i++) {
            if (fields[i] < obj.fields[i])
                return -1;
            else if (fields[i] > obj.fields[i])
                return 1;
        }
        return 0;
        
    }
    
    public String toString() {
        if (fields == null)
            return "";
        String t = String.valueOf(fields[0]); 
        for (int i = 1; i < length; i++) {
            t += "\t" + String.valueOf(fields[i]);
        }
        return t; 
    }
    
    /** A Comparator optimized for IntWritable. */ 
    public static class Comparator extends WritableComparator {
      public Comparator() {
        super(TimestampWritable.class);
      }

      public int compare(byte[] b1, int s1, int l1,
                         byte[] b2, int s2, int l2) {
          int i1, i2;
          for (int i = 0; i < TimestampWritable.length; i++) {
              i1 = readInt(b1, s1+ i*4);
              i2 = readInt(b2, s2+ i*4);
              
              if (i1 < i2) return -1;
              else if (i1 > i2) return 1;
          }
           
          return 0;
      }
    }

    static {                                        // register this comparator
      WritableComparator.define(TimestampWritable.class, new Comparator());
    }
    
}