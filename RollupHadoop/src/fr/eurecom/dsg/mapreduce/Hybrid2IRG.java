package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 * 
 */
public class Hybrid2IRG extends Configured implements Tool {

    private int numReducers;
	private Path inputPath;
	private Path outputDir;
	private int pivot;

	@Override
	public int run(String[] args) throws Exception {

		Configuration conf = this.getConf();
		Job job = new Job(conf, "Hung-Hybrid-2IRG-" + this.pivot); 
		
		// set job input format
		job.setInputFormatClass(SequenceFileInputFormat.class);

		// set map class and the map output key and value classes
		job.setMapperClass(Hybrid2IRGMapper.class);
		job.setMapOutputKeyClass(TimestampWritable.class);
		job.setMapOutputValueClass(LongWritable.class);
		
		job.setPartitionerClass(Hybrid2IRGPartitioner.class);

		// set reduce class and the reduce output key and value classes
		job.setReducerClass(Hybrid2IRGReducer.class);
		
		//job.setSortComparatorClass(TimestampWritable.Comparator.class);

		// set job output format
		job.setOutputKeyClass(TimestampWritable.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		job.setCombinerClass(Hybrid2IRGCombiner.class);
		

		// add the input file as job input (from HDFS) to the variable
		// inputFile
		FileInputFormat.addInputPath(job, inputPath);

		// set the output path for the job results (to HDFS) to the
		// variable
		// outputPath
		FileOutputFormat.setOutputPath(job, outputDir);

		// set the number of reducers using variable numberReducers
		job.setNumReduceTasks(this.numReducers);

		// set the jar class
		job.setJarByClass(Hybrid2IRG.class);
		
		
		Random tmp = new Random();
        int choosen = (tmp.nextInt() & Integer.MAX_VALUE);
		
		job.getConfiguration().set("hybrid.pivot", String.valueOf(pivot));
		job.getConfiguration().set("choosen", String.valueOf(choosen));

		return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
	}

	public Hybrid2IRG(String[] args) {
		if (args.length != 4) {
			System.out
					.println("Usage: HybridImplementation <input_path> <output_path> <pivot> <num_reducers>");
			System.exit(0);
		}
		this.inputPath = new Path(args[0]);
		this.outputDir = new Path(args[1]);
		this.pivot = Integer.parseInt(args[2]);
		this.numReducers = Integer.parseInt(args[3]);
	}

	public static void main(String args[]) throws Exception {
		int res = ToolRunner
				.run(new Configuration(), new Hybrid2IRG(args), args);
		System.exit(res);
	}

}


class Hybrid2IRGMapper extends Mapper<TimestampWritable, LongWritable, TimestampWritable, LongWritable> { 

	public Hybrid2IRGMapper() {

	}
	
	protected int pivot = -1;

	@Override
    protected void setup(org.apache.hadoop.mapreduce.Mapper<TimestampWritable, LongWritable, TimestampWritable, LongWritable>.Context context) throws IOException, InterruptedException {
        super.setup(context);
        pivot = Integer.parseInt(context.getConfiguration().get("hybrid.pivot", "-1"));
    }


    @Override
	protected void map(TimestampWritable key, 
	        LongWritable value,
			Context context) throws IOException, InterruptedException {
	    context.write(key, value);
	    
	    for (int i = pivot; i < TimestampWritable.length; i++) {
            key.fields[i] = TimestampWritable.NullValue;
        }
	
	    context.write(key, value);
    }

}

class Hybrid2IRGCombiner extends Reducer<TimestampWritable,
        LongWritable,
        TimestampWritable,
        LongWritable> {
    
    public Hybrid2IRGCombiner() {

    }

    @Override
    protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
            throws IOException, InterruptedException {
        // implement the reduce method (use context.write to emit results)
        long sum = 0;
        for (LongWritable lw : value) {
            sum += lw.get();
        }
        context.write(key, new LongWritable(sum));
    }
        
}

class Hybrid2IRGReducer extends Reducer<TimestampWritable,
        LongWritable, 
        TimestampWritable, 
        LongWritable> { 

    protected TimestampWritable previousKey;
    protected long[] tmpRes;
    protected TimestampWritable previousKey2;
    protected long[] tmpRes2;
    protected int pivot;
    protected boolean secondPass;
    
	public Hybrid2IRGReducer() {
	    previousKey = null;
	    tmpRes = new long[TimestampWritable.length];
	    previousKey2 = null;
	    tmpRes2 = new long[TimestampWritable.length];
	    secondPass = false;
	}
	
	@Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
	    //System.out.println("cleanup");
	    int x = TimestampWritable.NullValue;
        compute(new TimestampWritable(x, x, x, x, x, x), new LongWritable(0), context);
        if (secondPass && (pivot != 0)) {
            compute2(new TimestampWritable(x, x, x, x, x, x), new LongWritable(0), context);
            context.write(new TimestampWritable(x, x, x, x, x, x), new LongWritable(tmpRes2[0]));
        }
        super.cleanup(context);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        previousKey = new TimestampWritable(null);
        previousKey2 = new TimestampWritable(null);
        for (int i = 0; i < TimestampWritable.length; i++) {
            tmpRes[i] = 0;
            tmpRes2[i] = 0;
            
        }
        pivot = Integer.parseInt(context.getConfiguration().get("hybrid.pivot", "-1"));
    }
    
    protected void compute(TimestampWritable key, LongWritable value, Context context)
            throws IOException, InterruptedException {

        if (previousKey.fields != null) {
            LongWritable tmpVal = new LongWritable();
            for (int i = 0; i < TimestampWritable.length; i++) {
                if (previousKey.fields[i] != key.fields[i]) {
                    int x = Math.max(i, pivot);
                    for (int j = TimestampWritable.length - 2; j >= x; j--) {
                        tmpRes[j] += tmpRes[j+1];
                        tmpVal.set(tmpRes[j+1]);
                        tmpRes[j+1] = 0;
                        previousKey.fields[j+1] = TimestampWritable.NullValue;
                        //System.out.println("O1:\t" + previousKey + " / " + tmpVal);
                        context.write(previousKey, tmpVal);
                    }
                    break;
                }
                
            }
        }
        if (key.fields[0] != TimestampWritable.NullValue) {
            //System.out.println("O2:\t" + key + " / " + value);
            context.write(key, value);
            tmpRes[TimestampWritable.length - 1] += value.get();
            previousKey.fields = key.fields.clone();
        }
    }
    
    protected void compute2(TimestampWritable key, LongWritable value, Context context)
            throws IOException, InterruptedException {

        if (previousKey2.fields != null) {
            LongWritable tmpVal = new LongWritable();
            for (int i = 0; i < pivot; i++) {
                if (previousKey2.fields[i] != key.fields[i]) {
                    for (int j = pivot - 2; j >= i; j--) {
                        tmpRes2[j] += tmpRes2[j+1];
                        tmpVal.set(tmpRes2[j+1]);
                        tmpRes2[j+1] = 0;
                        previousKey2.fields[j+1] = TimestampWritable.NullValue;
                        //System.out.println("O1:\t" + previousKey + " / " + tmpVal);
                        context.write(previousKey2, tmpVal);
                    }
                    break;
                }
            }
        }
        
        if (key.fields[0] != TimestampWritable.NullValue) {
            //System.out.println("O2:\t" + key + " / " + value);
            context.write(key, value);
            tmpRes2[pivot - 1] += value.get();
            previousKey2.fields = key.fields.clone();
        } else if (pivot == 0) {
            context.write(key, value);
        }
    }

    @Override
	protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
			throws IOException, InterruptedException {
		// implement the reduce method (use context.write to emit results)
		long sum = 0;
		for (LongWritable lw : value) {
		    sum += lw.get();
		}
		if (key.fields[pivot] != TimestampWritable.NullValue) {
		    compute(key, new LongWritable(sum), context);
		} else {
		    secondPass = true;
		    compute2(key, new LongWritable(sum), context);
		}
	}
}
