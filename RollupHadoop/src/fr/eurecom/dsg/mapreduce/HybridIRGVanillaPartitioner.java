package fr.eurecom.dsg.mapreduce;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class HybridIRGVanillaPartitioner extends Partitioner<TimestampWritable, LongWritable> implements Configurable {
    
    protected MessageDigest m = null;
    protected int pivot = -1;
    protected int i;
    
    public HybridIRGVanillaPartitioner() throws NoSuchAlgorithmException {
         m = MessageDigest.getInstance("MD5");
    }
    
    @Override
    public void setConf(Configuration conf) {
        pivot = Integer.parseInt(conf.get("hybrid.pivot", "-1"));
    }

    @Override
    public Configuration getConf() {
        return null;
    }

    @Override
    public int getPartition(TimestampWritable key, LongWritable value, int numReduceTasks) {
        m.reset();
        for (i = 0; i <= pivot; i++) {
            m.update(ByteBuffer.allocate(4).putInt(key.fields[i]).array());
        }
        return (m.digest()[15] & Integer.MAX_VALUE) % numReduceTasks;
    }
}