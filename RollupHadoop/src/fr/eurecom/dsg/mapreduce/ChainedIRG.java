package fr.eurecom.dsg.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 * 
 */
public class ChainedIRG extends Configured implements Tool {

    private int numReducers;
	private Path inputPath;
	private Path outputDir;
	private int pivot;

	@Override
	public int run(String[] args) throws Exception {

		Configuration conf = this.getConf();
		Job job = new Job(conf, "Hung-ChainedIRG-" + this.pivot); 
		
		// set job input format
		job.setInputFormatClass(SequenceFileInputFormat.class);

		// set map class and the map output key and value classes
		job.setMapperClass(ChainedMRMapper.class);
		job.setMapOutputKeyClass(TimestampWritable.class);
		job.setMapOutputValueClass(LongWritable.class);
		
		job.setPartitionerClass(HybridIRGVanillaPartitioner.class);

		// set reduce class and the reduce output key and value classes
		job.setReducerClass(ChainedReducer1.class);
		
		//job.setSortComparatorClass(TimestampWritable.Comparator.class);

		// set job output format
		job.setOutputKeyClass(TimestampWritable.class);
		job.setOutputValueClass(LongWritable.class);
	
		MultipleOutputs.addNamedOutput(job, "tmp", SequenceFileOutputFormat.class, TimestampWritable.class, LongWritable.class);
		
		//job.setCombinerClass(ChainedCombiner.class);
		

		// add the input file as job input (from HDFS) to the variable
		// inputFile
		FileInputFormat.addInputPath(job, inputPath);

		// set the output path for the job results (to HDFS) to the
		// variable
		// outputPath
		FileOutputFormat.setOutputPath(job, outputDir);

		// set the number of reducers using variable numberReducers
		job.setNumReduceTasks(this.numReducers);

		// set the jar class
		job.setJarByClass(ChainedIRG.class);
		
		job.getConfiguration().set("hybrid.pivot", String.valueOf(pivot));

		return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
	}

	public ChainedIRG(String[] args) {
		if (args.length != 4) {
			System.out
					.println("Usage: ChainedMRImplementation <input_path> <output_path> <pivot> <num_reducers>");
			System.exit(0);
		}
		this.inputPath = new Path(args[0]);
		this.outputDir = new Path(args[1] + "/part1");
		this.pivot = Integer.parseInt(args[2]);
		this.numReducers = Integer.parseInt(args[3]);
	}

	public static void main(String args[]) throws Exception {
		int res = ToolRunner
				.run(new Configuration(), new ChainedIRG(args), args);
		
		String[] args2 = new String[] {args[1] + "/part1/tmp*", args[1] + "/part2", args[2]};
		res = ToolRunner
                .run(new Configuration(), new FinalAggregation(args2), args2);
		System.exit(res);
	}

}


class FinalAggregation extends Configured implements Tool {

    private Path inputPath;
    private Path outputDir;
    private int pivot;

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = this.getConf();
        Job job = new Job(conf, "FinalAggregation"); 
        
        // set job input format
        job.setInputFormatClass(SequenceFileInputFormat.class);

        // set map class and the map output key and value classes
        job.setMapperClass(ChainedMRMapper.class);
        job.setMapOutputKeyClass(TimestampWritable.class);
        job.setMapOutputValueClass(LongWritable.class);
        
        //job.setPartitionerClass(HashPartitioner.class);

        // set reduce class and the reduce output key and value classes
        job.setReducerClass(ChainedReducer2.class);
        
        //job.setSortComparatorClass(TimestampWritable.Comparator.class);

        // set job output format
        job.setOutputKeyClass(TimestampWritable.class);
        job.setOutputValueClass(LongWritable.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        
        //job.setCombinerClass(ChainedCombiner.class);
        

        // add the input file as job input (from HDFS) to the variable
        // inputFile
        FileInputFormat.addInputPath(job, inputPath);

        // set the output path for the job results (to HDFS) to the
        // variable
        // outputPath
        FileOutputFormat.setOutputPath(job, outputDir);

        // set the number of reducers using variable numberReducers
        job.setNumReduceTasks(1);

        // set the jar class
        job.setJarByClass(ChainedIRG.class);
        
        job.getConfiguration().set("hybrid.pivot", String.valueOf(pivot));

        return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
    }

    public FinalAggregation(String[] args) {
        if (args.length != 3) {
            System.out
                    .println("Usage: ChainedMRImplementation <input_path> <output_path> <pivot>");
            System.exit(0);
        }
        this.inputPath = new Path(args[0]);
        this.outputDir = new Path(args[1]);
        this.pivot = Integer.parseInt(args[2]);
    }
}


class ChainedMRMapper extends Mapper<TimestampWritable, LongWritable, TimestampWritable, LongWritable> { 

	public ChainedMRMapper() {

	}
	
	@Override
    protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context) throws IOException, InterruptedException {
        super.setup(context);
    }


    @Override
	protected void map(TimestampWritable key, 
	        LongWritable value,
			Context context) throws IOException, InterruptedException {
	    context.write(key, value);
	}

}

class ChainedCombiner extends Reducer<TimestampWritable,
        LongWritable,
        TimestampWritable,
        LongWritable> {
    
    public ChainedCombiner() {

    }

    @Override
    protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context)
            throws IOException, InterruptedException {
        // implement the reduce method (use context.write to emit results)
        long sum = 0;
        for (LongWritable lw : value) {
            sum += lw.get();
        }
        context.write(key, new LongWritable(sum));
    }
        
}


class ChainedReducer1 extends Reducer<TimestampWritable, LongWritable, TimestampWritable, LongWritable> {

    protected TimestampWritable previousKey;
    protected long[] tmpRes;
    protected int pivot;
    protected MultipleOutputs<TimestampWritable, LongWritable> mos;

    public ChainedReducer1() {
        previousKey = null;
        tmpRes = new long[TimestampWritable.length];
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        // System.out.println("cleanup");
        int x = TimestampWritable.NullValue;
        compute(new TimestampWritable(x, x, x, x, x, x), new LongWritable(0), context);
        super.cleanup(context);
        mos.close();
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        previousKey = new TimestampWritable(null);
        for (int i = 0; i < TimestampWritable.length; i++) {
            tmpRes[i] = 0;
        }
        pivot = Integer.parseInt(context.getConfiguration().get("hybrid.pivot", "-1"));
        //System.out.println("pivot: " + pivot);
        mos = new MultipleOutputs<TimestampWritable, LongWritable>(context);
    }

    protected void compute(TimestampWritable key, LongWritable value, Context context) throws IOException,
            InterruptedException {

        if (previousKey.fields != null) {
            LongWritable tmpVal = new LongWritable();
            for (int i = 0; i < TimestampWritable.length; i++) {
                if (previousKey.fields[i] != key.fields[i]) {
                    int x = Math.max(i, pivot);
                    for (int j = TimestampWritable.length - 2; j >= x; j--) {
                        tmpRes[j] += tmpRes[j + 1];
                        tmpVal.set(tmpRes[j+1]);
                        tmpRes[j + 1] = 0;
                        previousKey.fields[j + 1] = TimestampWritable.NullValue;
                        if (j == pivot) {                            
                            mos.write("tmp", previousKey, tmpVal);
                        } else {
                            context.write(previousKey, tmpVal);
                        }
                    }
                    break;
                }

            }
        }
        
        if (pivot == TimestampWritable.length - 1) {
            mos.write("tmp", key, value);
        } else if (key.fields[0] != TimestampWritable.NullValue) {
            // System.out.println("O2:\t" + key + " / " + value);
            context.write(key, value);
            tmpRes[TimestampWritable.length - 1] += value.get();
            previousKey.fields = key.fields.clone();
        }
        
    }

    @Override
    protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context) throws IOException,
            InterruptedException {
        // implement the reduce method (use context.write to emit results)
        long sum = 0;
        for (LongWritable lw : value) {
            sum += lw.get();
        }
        compute(key, new LongWritable(sum), context);
    }
}


class ChainedReducer2 extends Reducer<TimestampWritable, LongWritable, TimestampWritable, LongWritable> {

    protected TimestampWritable previousKey;
    
    protected long[] tmpRes;
    
    protected int pivot;

    public ChainedReducer2() {
        previousKey = null;
        tmpRes = new long[TimestampWritable.length];
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        // System.out.println("cleanup");
        int x = TimestampWritable.NullValue;
        compute(new TimestampWritable(x, x, x, x, x, x), new LongWritable(0), context);
        context.write(new TimestampWritable(x, x, x, x, x, x), new LongWritable(tmpRes[0]));
        super.cleanup(context);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        previousKey = new TimestampWritable(null);
        for (int i = 0; i < TimestampWritable.length; i++) {
            tmpRes[i] = 0;
        }
        pivot = Integer.parseInt(context.getConfiguration().get("hybrid.pivot", "-1"));
    }

    protected void compute(TimestampWritable key, LongWritable value, Context context) throws IOException,
            InterruptedException {

        if (previousKey.fields != null) {
            LongWritable tmpVal = new LongWritable();
            for (int i = 0; i < pivot; i++) {
                if (previousKey.fields[i] != key.fields[i]) {
                    for (int j = pivot - 1; j >= i; j--) {
                        tmpRes[j] += tmpRes[j + 1];
                        tmpVal.set(tmpRes[j+1]);
                        tmpRes[j + 1] = 0;
                        previousKey.fields[j + 1] = TimestampWritable.NullValue;
                        // System.out.println("O1:\t" + previousKey + " / " +
                        // tmpVal);
                        context.write(previousKey, tmpVal);
                    }
                    break;
                }

            }
        }
        if (key.fields[0] != TimestampWritable.NullValue) {
            // System.out.println("O2:\t" + key + " / " + value);
            context.write(key, value);
            tmpRes[pivot] += value.get();
            previousKey.fields = key.fields.clone();
        }
    }

    @Override
    protected void reduce(TimestampWritable key, Iterable<LongWritable> value, Context context) throws IOException,
            InterruptedException {
        // implement the reduce method (use context.write to emit results)
        long sum = 0;
        for (LongWritable lw : value) {
            sum += lw.get();
        }
        compute(key, new LongWritable(sum), context);
    }
}