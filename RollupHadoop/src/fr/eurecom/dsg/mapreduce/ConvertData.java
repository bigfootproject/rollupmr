package fr.eurecom.dsg.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 * 
 */
public class ConvertData extends Configured implements Tool {

    private Path inputPath;
    private Path outputDir;

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = this.getConf();
        Job job = new Job(conf, "ConvertData");

        // set job input format
        job.setInputFormatClass(TextInputFormat.class);

        // set map class and the map output key and value classes
        job.setMapperClass(ConvertMapper.class);
        job.setMapOutputKeyClass(TimestampWritable.class);
        job.setMapOutputValueClass(IntWritable.class);

        // set reduce class and the reduce output key and value classes
        job.setNumReduceTasks(0);

        // set job output format
        job.setOutputKeyClass(TimestampWritable.class);
        job.setOutputValueClass(LongWritable.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        // add the input file as job input (from HDFS) to the variable
        // inputFile
        FileInputFormat.addInputPath(job, inputPath);

        // set the output path for the job results (to HDFS) to the
        // variable
        // outputPath
        FileOutputFormat.setOutputPath(job, outputDir);

        // set the number of reducers using variable numberReducers
        job.setNumReduceTasks(0);

        // set the jar class
        job.setJarByClass(TestGroupBy.class);

        return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
    }

    public ConvertData(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: ConvertData <input_path> <output_path>");
            System.exit(0);
        }
        this.inputPath = new Path(args[0]);
        this.outputDir = new Path(args[1]);
    }

    public static void main(String args[]) throws Exception {
        int res = ToolRunner.run(new Configuration(), new ConvertData(args), args);
        System.exit(res);
    }

}


class ConvertMapper extends Mapper<LongWritable, Text, TimestampWritable, LongWritable> {

    public ConvertMapper() {

    }

    @Override
    protected void map(LongWritable key, // change Object to input key type
            Text value, // change Object to input value type
            Context context) throws IOException, InterruptedException {
        // implement the map method (use context.write to emit results)
        String line = value.toString();
        line = line.replaceAll("[^0-9]+", " ");
        String[] swords = line.split("\\s+");
                
        int year = Integer.parseInt(swords[1]);
        int month = Integer.parseInt(swords[2]);
        int day = Integer.parseInt(swords[3]);
        int hour = Integer.parseInt(swords[4]);
        int minute = Integer.parseInt(swords[5]);
        int second = Integer.parseInt(swords[6]);
        int payload = Integer.parseInt(swords[7]);
        
        TimestampWritable ts = new TimestampWritable(year, month, day, hour, minute, second);
        LongWritable pl = new LongWritable(payload);
        
        context.write(ts, pl);
        
    }

}

