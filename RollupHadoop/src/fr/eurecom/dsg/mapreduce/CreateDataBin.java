package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;



public class CreateDataBin {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Starting to write data");
		Configuration conf = new Configuration();
		conf.addResource(new Path("/etc/hadoop/conf.bigdoop/core-site.xml"));
		//conf.set("fs.defaultFS", "hdfs://10-10-10-23.openstacklocal:8000");
		FileSystem fs = FileSystem.get(conf);
		Path outFile = new Path(args[0]);
		SequenceFile.Writer writer = null;
		
		try {
		    Random random = new Random();
	        long dataSize = Long.parseLong(args[1]) * 1024 * 1024 * 1024;
	        int noYear = Integer.parseInt(args[2]);
	        long oneGB = 1024 * 1024 * 1024;
	        TimestampWritable key = new TimestampWritable();
	        LongWritable value = new LongWritable();
	        writer = SequenceFile.createWriter(fs, conf, outFile, key.getClass(), value.getClass());
	        while (true) {
	            int y = (int) (random.nextInt(1000000000) % noYear + 1990);
	            int m = random.nextInt(1000000000) % 12 + 1;
	            int d = random.nextInt(1000000000) % 31 + 1;
	            int h = random.nextInt(1000000000) % 24;
	            int MM = random.nextInt(1000000000) % 60;
	            int s = random.nextInt(1000000000) % 60;
	            long p = random.nextInt(1000000000) % 4096 + 1;
	            key.set(y, m, d, h, MM, s);
	            value.set(p);
	            writer.append(key, value);
	            
	            if (writer.getLength() + 64 > dataSize)
	                break;
	            
	            if (writer.getLength() % oneGB < 48) {
	                System.out.println((writer.getLength()/oneGB + 1) + "GB");
	            }
	        }
		} finally {
		    IOUtils.closeStream(writer);
		    System.out.println("Done");
		}
	
		
	}

}
